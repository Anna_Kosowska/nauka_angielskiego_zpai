<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210126194550 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, category_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE level (id INT AUTO_INCREMENT NOT NULL, level_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE progres (id INT AUTO_INCREMENT NOT NULL, progres_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE progres_user (progres_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_4DE6C4B67BFE8166 (progres_id), INDEX IDX_4DE6C4B6A76ED395 (user_id), PRIMARY KEY(progres_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE word (id INT AUTO_INCREMENT NOT NULL, category_id_id INT NOT NULL, level_id_id INT NOT NULL, word_eng VARCHAR(255) NOT NULL, word_pl VARCHAR(255) NOT NULL, INDEX IDX_C3F175119777D11E (category_id_id), INDEX IDX_C3F17511159D9B5E (level_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE word_progres (word_id INT NOT NULL, progres_id INT NOT NULL, INDEX IDX_6FCF810E357438D (word_id), INDEX IDX_6FCF8107BFE8166 (progres_id), PRIMARY KEY(word_id, progres_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE progres_user ADD CONSTRAINT FK_4DE6C4B67BFE8166 FOREIGN KEY (progres_id) REFERENCES progres (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE progres_user ADD CONSTRAINT FK_4DE6C4B6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE word ADD CONSTRAINT FK_C3F175119777D11E FOREIGN KEY (category_id_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE word ADD CONSTRAINT FK_C3F17511159D9B5E FOREIGN KEY (level_id_id) REFERENCES level (id)');
        $this->addSql('ALTER TABLE word_progres ADD CONSTRAINT FK_6FCF810E357438D FOREIGN KEY (word_id) REFERENCES word (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE word_progres ADD CONSTRAINT FK_6FCF8107BFE8166 FOREIGN KEY (progres_id) REFERENCES progres (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE word DROP FOREIGN KEY FK_C3F175119777D11E');
        $this->addSql('ALTER TABLE word DROP FOREIGN KEY FK_C3F17511159D9B5E');
        $this->addSql('ALTER TABLE progres_user DROP FOREIGN KEY FK_4DE6C4B67BFE8166');
        $this->addSql('ALTER TABLE word_progres DROP FOREIGN KEY FK_6FCF8107BFE8166');
        $this->addSql('ALTER TABLE progres_user DROP FOREIGN KEY FK_4DE6C4B6A76ED395');
        $this->addSql('ALTER TABLE word_progres DROP FOREIGN KEY FK_6FCF810E357438D');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE level');
        $this->addSql('DROP TABLE progres');
        $this->addSql('DROP TABLE progres_user');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE word');
        $this->addSql('DROP TABLE word_progres');
    }
}
