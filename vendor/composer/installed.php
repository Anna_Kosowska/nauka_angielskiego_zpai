<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'composer/package-versions-deprecated' => 
    array (
      'pretty_version' => '1.11.99.1',
      'version' => '1.11.99.1',
      'aliases' => 
      array (
      ),
      'reference' => '7413f0b55a051e89485c5cb9f765fe24bb02a7b6',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.11.1',
      'version' => '1.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ce77a7ba1770462cd705a91a151b6c3746f9c6ad',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => '1.10.2',
      'version' => '1.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '13e3381b25847283a91948d04640543941309727',
    ),
    'doctrine/collections' => 
    array (
      'pretty_version' => '1.6.7',
      'version' => '1.6.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '55f8b799269a1a472457bd1a41b4f379d4cfba4a',
    ),
    'doctrine/common' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2afde5a9844126bc311cd5f548b5475e75f800d3',
    ),
    'doctrine/data-fixtures' => 
    array (
      'pretty_version' => '1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '51d3d4880d28951fff42a635a2389f8c63baddc5',
    ),
    'doctrine/dbal' => 
    array (
      'pretty_version' => '2.12.1',
      'version' => '2.12.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'adce7a954a1c2f14f85e94aed90c8489af204086',
    ),
    'doctrine/doctrine-bundle' => 
    array (
      'pretty_version' => '2.2.3',
      'version' => '2.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '015fdd490074d4daa891e2d1df998dc35ba54924',
    ),
    'doctrine/doctrine-fixtures-bundle' => 
    array (
      'pretty_version' => '3.4.0',
      'version' => '3.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '870189619a7770f468ffb0b80925302e065a3b34',
    ),
    'doctrine/doctrine-migrations-bundle' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b8de89fe811e62f1dea8cf9aafda0ea45ca6f1f3',
    ),
    'doctrine/event-manager' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41370af6a30faa9dc0368c4a6814d596e81aba7f',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9cf661f4eb38f7c881cac67c75ea9b00bf97b210',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd56bf6102915de5702778fe20f2de3b2fe570b5b',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'doctrine/migrations' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '6195e836ffc2e1bd5ddea468fa46015fbea00b3a',
    ),
    'doctrine/orm' => 
    array (
      'pretty_version' => '2.8.1',
      'version' => '2.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '242cf1a33df1b8bc5e1b86c3ebd01db07851c833',
    ),
    'doctrine/persistence' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9899c16934053880876b920a3b8b02ed2337ac1d',
    ),
    'doctrine/sql-formatter' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '56070bebac6e77230ed7d306ad13528e60732871',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '2.1.25',
      'version' => '2.1.25.0',
      'aliases' => 
      array (
      ),
      'reference' => '0dbf5d78455d4d6a41d186da50adc1122ec066f4',
    ),
    'friendsofphp/proxy-manager-lts' => 
    array (
      'pretty_version' => 'v1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '121af47c9aee9c03031bdeca3fac0540f59aa5c3',
    ),
    'laminas/laminas-code' => 
    array (
      'pretty_version' => '3.4.1',
      'version' => '3.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1cb8f203389ab1482bf89c0e70a04849bacd7766',
    ),
    'laminas/laminas-eventmanager' => 
    array (
      'pretty_version' => '3.3.0',
      'version' => '3.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1940ccf30e058b2fd66f5a9d696f1b5e0027b082',
    ),
    'laminas/laminas-zendframework-bridge' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '6ede70583e101030bcace4dcddd648f760ddf642',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1cb1cde8e8dd0f70cc0fe51354a59acad9302084',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.10.4',
      'version' => '4.10.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6d052fc58cb876152f89f532b95a8d7907e7f0e',
    ),
    'ocramius/package-versions' => 
    array (
      'replaced' => 
      array (
        0 => '1.11.99',
      ),
    ),
    'ocramius/proxy-manager' => 
    array (
      'replaced' => 
      array (
        0 => '^2.1',
      ),
    ),
    'paragonie/random_compat' => 
    array (
      'replaced' => 
      array (
        0 => '2.*',
      ),
    ),
    'php-http/async-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '*',
      ),
    ),
    'php-http/client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '*',
      ),
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.2.2',
      'version' => '5.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '069a785b2141f5bcf49f3e353548dc1cce6df556',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6a467b8989322d92aa1c8bf2bebcc6e5c2ba55c0',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/link' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eea8e8662d5cd3ae4517c9b864493f59fca95562',
    ),
    'psr/link-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
        1 => '1.0',
      ),
    ),
    'psr/simple-cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'sensio/framework-extra-bundle' => 
    array (
      'pretty_version' => 'v5.6.1',
      'version' => '5.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '430d14c01836b77c28092883d195a43ce413ee32',
    ),
    'symfony/apache-pack' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3aa5818d73ad2551281fc58a75afd9ca82622e6c',
    ),
    'symfony/asset' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aaf4ba865c02f6df999166a0148d56f2b11b11fb',
    ),
    'symfony/browser-kit' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c46b676a993cc437bafe6fe0f30f074857cde2a6',
    ),
    'symfony/cache' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '5da40a385c8182d18f4cca960bce7191c8f24e07',
    ),
    'symfony/cache-contracts' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8034ca0b61d4dd967f3698aaa1da2507b631d0cb',
    ),
    'symfony/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'symfony/config' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '2306321ef6a21a0de51a139774b6b7b38804815b',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '95794074741645473221fb126d5cb4057ad25bf1',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '79c224cdbfae58d54b257a8c684ad445042c90f2',
    ),
    'symfony/debug-bundle' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '4bae28a913fa32ec123a37b3178b7b7d3a4ac323',
    ),
    'symfony/dependency-injection' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '9263d52372205c57823bf983bc4f413378830757',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5fa56b4074d1ae755beb55617ddafe6f5d78f665',
    ),
    'symfony/doctrine-bridge' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e2ab3fe26133c5d997684f1b961acbd6b04e2805',
    ),
    'symfony/dom-crawler' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bbc756c0895d08a1e69a59d8541a647b47f5a732',
    ),
    'symfony/dotenv' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'efd887f012127acad22325d109fe8ddf635f1f97',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd01fba9a55614a1addb0d52d6a9566560b2a2af8',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '5c5dd86c7a7962d28c48351c7dd83c9266e4d19d',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0ba7d54483095a198fa51781bc608d17e84dffa2',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/expression-language' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '31ea3085d94d2656a3560ba303e0e27456c5d265',
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '6edf8b9e64e662fcde20ee3ee2ec46fdcc8c3214',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '127bccabf3c854625af9c0162779cf06bc1dd352',
    ),
    'symfony/flex' => 
    array (
      'pretty_version' => 'v1.11.0',
      'version' => '1.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ceb2b4e612bd0b4bb36a4d7fb2e800c861652f48',
    ),
    'symfony/form' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '7c4aee4717558389cbfea35fa84d8dd830965db1',
    ),
    'symfony/framework-bundle' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '0fc0a93f8bbe465d0b483e21b087d432baa92c16',
    ),
    'symfony/http-client' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '9eec6ed50ea38f562ce0a1fc8a7d96a010d58509',
    ),
    'symfony/http-client-contracts' => 
    array (
      'pretty_version' => 'v2.3.1',
      'version' => '2.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41db680a15018f9c1d4b23516059633ce280ca33',
    ),
    'symfony/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1',
      ),
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '7ad89bbacd90f7bee1a57e61ed5ecaeaba430706',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '410ce82fbbb06fb926ecaacea8b0af86bc3e7ef2',
    ),
    'symfony/inflector' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '7eff2643934179cd0e5a6609a583fc22fc495fc4',
    ),
    'symfony/intl' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '72cfa77bde9d3fdb97eaf04933951d87f999d774',
    ),
    'symfony/mailer' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae0579ff80c1f9b6db5a7a7053733b2568cb9001',
    ),
    'symfony/maker-bundle' => 
    array (
      'pretty_version' => 'v1.28.0',
      'version' => '1.28.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6f4d27a68c92179c124f5331a27e32d197c9bd59',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aa2b2013a8d380e3980a29a79cc0fbcfb02fb920',
    ),
    'symfony/monolog-bridge' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd48bf711b47c6fa9a0df747a73ad4d45d8fc5409',
    ),
    'symfony/monolog-bundle' => 
    array (
      'pretty_version' => 'v3.6.0',
      'version' => '3.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e495f5c7e4e672ffef4357d4a4d85f010802f940',
    ),
    'symfony/notifier' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e5d0bb4a54509f817814cb4f267c8426e65396e4',
    ),
    'symfony/options-resolver' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '9f39c468be4b6dae1bad2422f98bab65734055e2',
    ),
    'symfony/phpunit-bridge' => 
    array (
      'pretty_version' => 'v5.2.1',
      'version' => '5.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '235823f6d215c9bd930a47a496e62c1354cde55b',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-iconv' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '267a9adeb8ecb8071040a740930e077cdfb987af',
    ),
    'symfony/polyfill-intl-icu' => 
    array (
      'pretty_version' => 'v1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b2b1e732a6c039f1a3ea3414b3379a2433e183d6',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0eb8293dbbcd6ef6bf81404c9ce7d95bcdf34f44',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6e971c891537eb617a00bb07a43d182a6915faba',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f377a3dd1fde44d37b9831d68dc8dea3ffd28e13',
    ),
    'symfony/polyfill-php56' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php70' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php71' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php72' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a678b42e92f86eca04b7fa4c0f6f19d097fb69e2',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dc3063ba22c2a1fd2f45ed856374d79114998f91',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f1052340d8832a5ee55f4160e651a88219a1499e',
    ),
    'symfony/property-access' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fdc47c3780ebb29077c3421c6253ccc91040c24a',
    ),
    'symfony/property-info' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ab2210c90e8038ffaad09fe10cf635ad31bebb62',
    ),
    'symfony/proxy-manager-bridge' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c9279782d3282fe69aff55dbe8d24939cac88f57',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '1369ee6823074c406815b65a40d47fd5ee48e517',
    ),
    'symfony/security-bundle' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '8e8b20291be3b4f9aed4da706450dc355ee036ac',
    ),
    'symfony/security-core' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '4e4c76fcb091e35aca0601fc337f0c2cf76885ab',
    ),
    'symfony/security-csrf' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '155a413dc29400e74d2c06f5581da795200386c1',
    ),
    'symfony/security-guard' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d920d91fa44be8ebfe1a101dadde48181d8a4fb',
    ),
    'symfony/security-http' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e18913e3663dde1d4712c921211d12185c323c6e',
    ),
    'symfony/serializer' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '825b66f545da95e9bb1626d5655be6693376d52a',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd15da7ba4957ffb8f1747218be9e1a121fd298a1',
    ),
    'symfony/service-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'symfony/stopwatch' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fbc3084469450c6f6616f5436a00e180ea9ff118',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd9090857c51a0ded54a54a9ed1121af24f0322f4',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '762090e92d8df2b91cace8930ce0329674600225',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e2eaa60b558f26a4b0354e1bbb25636efaaad105',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/twig-bridge' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '293e5f04eee4da963686beab20960b45e4db68ad',
    ),
    'symfony/twig-bundle' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '348863cd784b10ea7e1485dc3003c738c6cdf547',
    ),
    'symfony/validator' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '9084a603e998e7abb010c44f8e3a2046b61fa9f6',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '36d19dbb4b377273dddb820adcdf0cc9dcf57731',
    ),
    'symfony/var-exporter' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b87e3aeedb74ee2694932d04153df9d804954cc3',
    ),
    'symfony/web-link' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '1b2e3621074e65632f9690c4d0cb59da8e71b4fc',
    ),
    'symfony/web-profiler-bundle' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '3b6dbd2cc76275e117d5c55923c7f511ead22bae',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v5.0.11',
      'version' => '5.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '29b60e88ff11a45b708115004fdeacab1ee3dd5d',
    ),
    'twig/extra-bundle' => 
    array (
      'pretty_version' => 'v3.2.1',
      'version' => '3.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '07c94c7dcfe7e49abd45d4083ca5544a34969714',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v3.2.1',
      'version' => '3.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f795ca686d38530045859b0350b5352f7d63447d',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.9.1',
      'version' => '1.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bafc69caeb4d49c39fd0779086c03a3738cbb389',
    ),
    'zendframework/zend-code' => 
    array (
      'replaced' => 
      array (
        0 => '3.4.1',
      ),
    ),
    'zendframework/zend-eventmanager' => 
    array (
      'replaced' => 
      array (
        0 => '^3.2.1',
      ),
    ),
  ),
);
