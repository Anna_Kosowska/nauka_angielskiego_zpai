<?php

namespace App\Entity;

use App\Repository\ProgresRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProgresRepository::class)
 */
class Progres
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $progres_id;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="progres")
     */
    private $user_id;

    /**
     * @ORM\ManyToMany(targetEntity=Word::class, mappedBy="word_id")
     */
    private $word_id;

    public function __construct()
    {
        $this->user_id = new ArrayCollection();
        $this->word_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProgresId(): ?int
    {
        return $this->progres_id;
    }

    public function setProgresId(int $progres_id): self
    {
        $this->progres_id = $progres_id;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUserId(): Collection
    {
        return $this->user_id;
    }

    public function addUserId(User $userId): self
    {
        if (!$this->user_id->contains($userId)) {
            $this->user_id[] = $userId;
        }

        return $this;
    }

    public function removeUserId(User $userId): self
    {
        $this->user_id->removeElement($userId);

        return $this;
    }

    /**
     * @return Collection|Word[]
     */
    public function getWordId(): Collection
    {
        return $this->word_id;
    }

    public function addWordId(Word $wordId): self
    {
        if (!$this->word_id->contains($wordId)) {
            $this->word_id[] = $wordId;
            $wordId->addWordId($this);
        }

        return $this;
    }

    public function removeWordId(Word $wordId): self
    {
        if ($this->word_id->removeElement($wordId)) {
            $wordId->removeWordId($this);
        }

        return $this;
    }
}
