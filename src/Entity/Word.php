<?php

namespace App\Entity;

use App\Repository\WordRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WordRepository::class)
 */
class Word
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=Progres::class, inversedBy="word_id")
     */
    private $word_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $word_ENG;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $word_PL;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="category_id")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category_id;

    /**
     * @ORM\ManyToOne(targetEntity=Level::class, inversedBy="level_id")
     * @ORM\JoinColumn(nullable=false)
     */
    private $level_id;

    public function __construct()
    {
        $this->word_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Progres[]
     */
    public function getWordId(): Collection
    {
        return $this->word_id;
    }

    public function addWordId(Progres $wordId): self
    {
        if (!$this->word_id->contains($wordId)) {
            $this->word_id[] = $wordId;
        }

        return $this;
    }

    public function removeWordId(Progres $wordId): self
    {
        $this->word_id->removeElement($wordId);

        return $this;
    }

    public function getWordENG(): ?string
    {
        return $this->word_ENG;
    }

    public function setWordENG(string $word_ENG): self
    {
        $this->word_ENG = $word_ENG;

        return $this;
    }

    public function getWordPL(): ?string
    {
        return $this->word_PL;
    }

    public function setWordPL(string $word_PL): self
    {
        $this->word_PL = $word_PL;

        return $this;
    }

    public function getCategoryId(): ?Category
    {
        return $this->category_id;
    }

    public function setCategoryId(?Category $category_id): self
    {
        $this->category_id = $category_id;

        return $this;
    }

    public function getLevelId(): ?Level
    {
        return $this->level_id;
    }

    public function setLevelId(?Level $level_id): self
    {
        $this->level_id = $level_id;

        return $this;
    }
}
