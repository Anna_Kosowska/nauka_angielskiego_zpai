<?php

namespace App\Entity;

use App\Repository\LevelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LevelRepository::class)
 */
class Level
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $level_name;

    /**
     * @ORM\OneToMany(targetEntity=Word::class, mappedBy="level_id")
     */
    private $level_id;

    public function __construct()
    {
        $this->level_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLevelName(): ?string
    {
        return $this->level_name;
    }

    public function setLevelName(string $level_name): self
    {
        $this->level_name = $level_name;

        return $this;
    }

    /**
     * @return Collection|Word[]
     */
    public function getLevelId(): Collection
    {
        return $this->level_id;
    }

    public function addLevelId(Word $levelId): self
    {
        if (!$this->level_id->contains($levelId)) {
            $this->level_id[] = $levelId;
            $levelId->setLevelId($this);
        }

        return $this;
    }

    public function removeLevelId(Word $levelId): self
    {
        if ($this->level_id->removeElement($levelId)) {
            // set the owning side to null (unless already changed)
            if ($levelId->getLevelId() === $this) {
                $levelId->setLevelId(null);
            }
        }

        return $this;
    }
}
