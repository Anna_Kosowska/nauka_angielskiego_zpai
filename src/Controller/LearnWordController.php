<?php

namespace App\Controller;

use App\Entity\Word;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LearnWordController extends AbstractController
{
    /**
     * @Route("/learn", name="learn")
     */

    public function learn_word(): Response
    {
        $i=2;
        $em=$this->getDoctrine()->getManager();

        //for($i=1; $i < 100; $i++) {
            $word=$em->getRepository(Word::class)->find($i);
       // }

        return $this->render('learn_word/learn_word.html.twig', ['word'=>$word]);

    }
    /**
     * @Route("/sprawdzenie", name="sprawdzenie")
     */
    public function check(): Response
    {


        $i = 2;
        $em = $this->getDoctrine()->getManager();
        $word =$em->getRepository(Word::class)->find($i);
        //$wordObject = word.getWordPL();

        $slowoplo = $_GET['slowopl'];
        if ($word == $slowoplo):
            {


                $this->addFlash('success', 'Slowo poprawne!');
            }
        else:{
                $this->addFlash('error', 'Słowo błędne!');
            }
           endif;


        return $this->redirectToRoute('learn');
    }

}