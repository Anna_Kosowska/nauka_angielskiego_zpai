<?php

namespace App\Controller;
use App\Entity\Word;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class LearningWordsController extends AbstractController
{
    /**
     * @Route("/words", name="words")
     */
    public function index():Response
    {
        $em=$this->getDoctrine()->getManager();
        $showWord=$em->getRepository(Word::class);
        return $this->render('learning_words/learning_words.html.twig',[
            'showWord'=>$showWord
        ]);

    }

}